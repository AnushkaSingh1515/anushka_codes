import random
def play() -> str:
    diamond_cards = [1,2,3,4,5,6,7,8,9,10,11,12,13]
    user_cards = [1,2,3,4,5,6,7,8,9,10,11,12,13]
    comp_cards = [1,2,3,4,5,6,7,8,9,10,11,12,13]
    comp_score = 0
    user_score = 0
    while(len(diamond_cards)>0):
        diamond_throws = random.choice(diamond_cards)
        diamond_cards.remove(diamond_throws)
        print("diamond threw",diamond_throws)
        print("you can throw",user_cards)
        user_throws = int(input("enter what you want to throw"))
        user_cards.remove(user_throws)
        print("user threw",user_throws)
        computer_throws = choose_computer(diamond_throws,comp_cards)
        comp_cards.remove(computer_throws)
        print("computer threw",computer_throws)
        winning = cal_score(diamond_throws,user_throws,computer_throws,user_score,comp_score)
        print("this round is won by",winning)
    return winning

def choose_computer(diamond_throws: int,comp_cards:list[int]) -> int:
    for i in comp_cards:
        if i>diamond_throws:
            return i
    return comp_cards[len(comp_cards)-1]

def cal_score(diamond_throws: int, user_throws: int,computer_throws: int,comp_score:int,user_score:int):
    if(user_throws == computer_throws):
        user_score += diamond_throws/2
        comp_score += diamond_throws/2
    if(user_throws>computer_throws):
        user_score += diamond_throws
    if(computer_throws > user_throws):
        comp_score += diamond_throws

    if(user_score > comp_score):
        return "user"
    if(user_score == comp_score):
        return "none"
    else:
        return "computer"

print(play())

